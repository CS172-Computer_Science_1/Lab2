#include <iostream>
//#include <sstream>
#include <cmath>
#include <iomanip>
#include <string>
#include "lab2_ex03.h" 
#include "lab2_ex17.h" 
#include "lab2_ex24.h"
using namespace std;

int main(){
	string strNotice = ".:[Please enter exercise number: >=>=>=> (3, 17, 24)]:.\n";
	int inputN;

	cout << "Lab 2 Exercises:\n\n";

	for (int iFor=0;iFor<10;iFor++){ //<-Prevent Infinite Loop
		cout << strNotice << endl;
		cin >> inputN;
		cin.ignore();
		cout << "+-=[You selected: (" << inputN << ")]=-+" << endl;
	
		if (inputN == 3){
			cout << "\n####Lab 2, Exercise 3####\n";
			nex03::dblex03();
		}
		else if (inputN == 17){
			cout << "\n####Lab 2, Exercise 17####\n";
			nex17::intex17();
		}
		else if (inputN == 24){
			cout << "\n####Lab 2, Exercise 24####\n";
			nex24::intex24();
		}
		else{
			cout << "Try again!!! \n\n";
			cin.clear();   //Clear Input
			cin.ignore();  //Prevent Letter Loop
		}
	}

}