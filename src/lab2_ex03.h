#include <iostream>
#include <iomanip>
using namespace std;

namespace nex03{
	double dblex03(){
		double T1, T2, T3, T4, T5, Avg;

		cout << "Calculator: Test Average (5 tests)\n\n";

		cout << "Input Test 1 Score: \n";
		cin >> T1;
		cout << "Input Test 2 Score: \n";
		cin >> T2;
		cout << "Input Test 3 Score: \n";
		cin >> T3;
		cout << "Input Test 4 Score: \n";
		cin >> T4;
		cout << "Input Test 5 Score: \n";
		cin >> T5;
		Avg = (T1 + T2 + T3 + T4 + T5) / 5;
		cout << "Your Average Score is: " << setprecision(1) << fixed << Avg << endl << endl;
		return 0;
	}	
}
