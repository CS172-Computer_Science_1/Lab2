#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

namespace nex17{
	int intex17(){
		int nr1=0, nr2=0, sumtotal=0; 
		const int min = 1, max=500;
		unsigned seed = time(0);

		srand(seed);

		cout << "Calculator: Sum of Two Numbers\n\n";
		nr1 = (rand() % (max - min + 1) + min);
		nr2 = (rand() % (max - min + 1) + min);
		cout << "First Number: " << cout.fill('.') << setw(10) << nr1 << endl;
		cout << "Second Number: " << cout.fill('.') << setw(9) << nr2 << endl;

		cout << "Press <Enter> after manually trying to solve, to see the answer.\n";
		cin.ignore();

		sumtotal = nr1 + nr2;
		cout << "The sum of both numbers is: "<< sumtotal << endl << endl;

		return 0;
	}
}