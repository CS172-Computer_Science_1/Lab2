#include <iostream>
#include <string>
using namespace std;

namespace nex24{
	int intex24(){
		//int age;
		string name, age, city, college, prof, animal, pet;
		
		cout << "I want to tell you a fancy story, but first i need some info.\n\n";
		cout << "What's your <name>?\n";
		getline(cin, name);	
		cout << "How <old> are you?\n";
		getline(cin, age);
		cout << "What <city> do you live in?\n";
		getline(cin, city);
		cout << "What <college> do you attend?\n";
		getline(cin, college);
		cout << "Spartan, what is your <profession>?\n";
		getline(cin, prof);
		cout << "What is your <favorite animal>?\n";
		getline(cin, animal);
		cout << "If you have/want a pet, what would be it's <pet name>?\n";
		getline(cin, pet);

		cout << "Press <Enter> to see the story.\n";
		cin.ignore();

		cout << "There once was a person named <" << name << "> who lived in <" << city << ">. At the age of \n"
			"<"<< age << ">, <" << name << "> went to college at <" << college << ">. <" << name << "> graduated and went to work\n"
			"as a <" << prof << ">. Then, <" << name << "> adopted a(n) <" << animal << "> named <" << pet << ">. They\n"
			"both lived happily ever after!\n\n";

		return 0;
	}
}